<?php

declare(strict_types=1);

use Diglactic\Breadcrumbs\Breadcrumbs;
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;

Breadcrumbs::for('login', function (BreadcrumbTrail $trail) {
    //
});

Breadcrumbs::for('dashboard', function (BreadcrumbTrail $trail) {
    //
});

// Modul Akreditasi
Breadcrumbs::for('modules.accreditations.index', function (BreadcrumbTrail $trail) {
    $trail->push('Dashboard', route('modules.accreditations.index'));
});

Breadcrumbs::for('modules.accreditations.unit.index', function (BreadcrumbTrail $trail) {
    $trail->push('Dashboard', route('modules.accreditations.index'));
    $trail->push('Unit', route('modules.accreditations.unit.index'));
});

Breadcrumbs::for('modules.accreditations.live.index', function (BreadcrumbTrail $trail) {
    $trail->push('Dashboard', route('modules.accreditations.index'));
    $trail->push('Akreditasi Berjalan', route('modules.accreditations.live.index'));
});

Breadcrumbs::for('modules.accreditations.history.index', function (BreadcrumbTrail $trail) {
    $trail->push('Dashboard', route('modules.accreditations.index'));
    $trail->push('Histori Akreditasi', route('modules.accreditations.history.index'));
});

// Modul Notifikasi
Breadcrumbs::for('modules.notifications.index', function (BreadcrumbTrail $trail) {
    $trail->push('Dashboard', route('modules.notifications.index'));
});

Breadcrumbs::for('modules.notifications.schedule', function (BreadcrumbTrail $trail) {
    $trail->push('Dashboard', route('modules.notifications.index'));
    $trail->push('Jadwal Notifikasi', route('modules.notifications.schedule'));
});
