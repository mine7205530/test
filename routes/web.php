<?php

declare(strict_types=1);

use App\Http\Controllers\AccreditationController;
use App\Http\Controllers\AccreditationDashboardController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HistoryAccreditationController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\NotificationsDashboardController;
use App\Http\Controllers\UnitController;
use Illuminate\Foundation\Http\Middleware\HandlePrecognitiveRequests;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth'])->group(function () {
    Route::get('/', DashboardController::class)->name('dashboard');
});

Route::prefix('modul')->as('modules.')->middleware(['auth'])->group(function () {
    Route::prefix('akreditasi')->as('accreditations.')->group(function () {
        Route::get('/', AccreditationDashboardController::class)->name('index');
        Route::prefix('unit')->as('unit.')->group(function () {
            Route::get('/', [UnitController::class, 'index'])->name('index');
            Route::post('/', [UnitController::class, 'store'])->name('store')->middleware([HandlePrecognitiveRequests::class]);
            Route::put('/{unit}', [UnitController::class, 'update'])->name('update')->middleware([HandlePrecognitiveRequests::class]);
            Route::delete('/{unit}', [UnitController::class, 'destroy'])->name('destroy');
        });
        Route::prefix('berjalan')->as('live.')->group(function () {
            Route::get('/', [AccreditationController::class, 'index'])->name('index');
            Route::post('/', [AccreditationController::class, 'store'])->name('store')->middleware([HandlePrecognitiveRequests::class]);
            Route::delete('/{accreditation}', [AccreditationController::class, 'destroy'])->name('destroy');
            Route::put('/{accreditation}', [AccreditationController::class, 'update'])->name('update');
            Route::put('/{accreditation}/deactivate', [AccreditationController::class, 'deactivate'])->name('deactivate');
        });
        Route::prefix('histori')->as('history.')->group(function () {
            Route::get('/', [HistoryAccreditationController::class, 'index'])->name('index');
            Route::delete('/{accreditation}', [HistoryAccreditationController::class, 'destroy'])->name('destroy');
        });
    });
    Route::prefix('notifikasi')->as('notifications.')->group(function () {
        Route::get('/', NotificationsDashboardController::class)->name('index');
        Route::get('/jadwal', [NotificationController::class, 'schedule'])->name('schedule');
        Route::post('/jadwal/monev', [NotificationController::class, 'scheduleMonev'])
            ->middleware([HandlePrecognitiveRequests::class])->name('schedule.monev');
    });
    Route::prefix('pengguna')->as('users.')->group(function () {
        Route::get('/', function () {
            return inertia('Modules/Pengguna/Index');
        })->name('index');
    });
});
