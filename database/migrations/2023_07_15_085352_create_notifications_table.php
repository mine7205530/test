<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->enum('type', ['Masa Kadaluarsa PT', 'Masa Kadaluarsa', 'Monev Data', 'Persiapan Pemantauan', 'Hasil Pemantauan', 'Persiapan Reakreditasi']);
            $table->date('due_date');
            $table->enum('status', ['Terkirim', 'Belum Terkirim', 'Gagal Terkirim'])->default('Belum Terkirim');
            $table->foreignUuid('accreditation_id')->constrained()->cascadeOnDelete();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('notifications');
    }
};
