<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('units', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name', 50);
            $table->text('description')->nullable();
            $table->enum('degree', ['D3', 'D4', 'S1', 'S2', 'S3', 'PT', 'PPAk']);
            $table->enum('status', ['Aktif', 'Tidak Aktif', 'Pantau I', 'Pantau II', 'Pantau III'])->default('Tidak Aktif');
            $table->string('email', 50)->unique();
            $table->string('sk_number')->unique();
            $table->string('sk_unit')->nullable()->default('sample.pdf');
            $table->foreignUuid('faculty_id')->constrained()->cascadeOnDelete();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('units');
    }
};
