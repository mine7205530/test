<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('accreditations', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->enum('grade', ['A', 'B', 'C', 'Unggul', 'Baik Sekali', 'Baik']);
            $table->enum('institution', ['BAN-PT', 'LAMEMBA', 'LAMINFOKOM', 'LAMTEKNIK', 'LAMDIK', 'LAMSAMA']);
            $table->enum('status', ['Aktif', 'Tidak Aktif', 'Pantau I', 'Pantau II', 'Pantau III'])->default('Aktif');
            $table->date('validity_date');
            $table->string('sk_number')->unique();
            $table->string('sk_accreditation', 255)->nullable()->default('sample.pdf');
            $table->foreignUuid('unit_id')->constrained()->cascadeOnDelete();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('accreditations');
    }
};
