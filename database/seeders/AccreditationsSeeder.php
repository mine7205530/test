<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Enums\Degree;
use App\Enums\StatusAccreditation;
use App\Models\Unit;
use Illuminate\Database\Seeder;

class AccreditationsSeeder extends Seeder
{
    public function run(): void
    {
        $units = Unit::query()
            ->get();

        foreach ($units as $unit) {
            if ($unit->degree === Degree::PT) {
                $validityDate = fake()->dateTimeBetween('+1 years', '+4 years');
            } else {
                $validityDate = fake()->dateTimeBetween('-1 years', '+4 years');
            }

            $unit->accreditations()
                ->create([
                    'sk_number' => 'SK-0017/UN.5.1/PT/'.$unit->name.'/'.$unit->degree->value,
                    'grade' => fake()->randomElement(['A', 'B', 'C', 'Unggul', 'Baik Sekali', 'Baik']),
                    'institution' => fake()->randomElement(['BAN-PT', 'LAMEMBA', 'LAMINFOKOM', 'LAMTEKNIK', 'LAMDIK', 'LAMSAMA']),
                    'status' => $validityDate > now() ? StatusAccreditation::AKTIF : StatusAccreditation::TIDAK_AKTIF,
                    'validity_date' => $validityDate,
                ]);

            $unit->update([
                'status' => $validityDate > now() ? StatusAccreditation::AKTIF : StatusAccreditation::TIDAK_AKTIF,
            ]);
        }
    }
}
