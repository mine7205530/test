<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    use WithoutModelEvents;

    public function run(): void
    {
        $this->call([
            UsersSeeder::class,
            FacultiesSeeder::class,
            UnitsSeeder::class,
            AccreditationsSeeder::class,
            NotificationsSeeder::class,
        ]);
    }
}
