<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Enums\Degree;
use App\Enums\StatusAccreditation;
use App\Enums\StatusNotification;
use App\Enums\TypeNotification;
use App\Models\Accreditation;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Seeder;

class NotificationsSeeder extends Seeder
{
    public function run(): void
    {
        $accreditations = Accreditation::query()
            ->whereNot('status', '=', StatusAccreditation::TIDAK_AKTIF)
            ->get();

        $validityDatePT = Accreditation::query()
            ->whereHas('unit', fn (Builder $query) => $query->where('degree', '=', Degree::PT))
            ->first()
            ->validity_date;

        // Masa Berlaku Akreditasi Tahunan
        foreach ($accreditations as $accreditation) {
            if ($accreditation->validity_date->subYear() > now()) {
                $accreditation->notifications()->create([
                    'type' => TypeNotification::MASA_KADALUARSA,
                    'status' => StatusNotification::BELUM_TERKIRIM,
                    'due_date' => $accreditation->validity_date->subYear(),
                ]);
            }
        }

        // Masa Berlaku PT
        $dueDatePT = $validityDatePT->subYear();
        foreach ($accreditations as $accreditation) {
            if ($accreditation->unit->degree !== Degree::PT) {
                $accreditation->notifications()->create([
                    'type' => TypeNotification::MASA_KADALUARSA_PT,
                    'status' => StatusNotification::BELUM_TERKIRIM,
                    'due_date' => $dueDatePT,
                ]);
            }
        }

    }
}
