<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Faculty;
use Illuminate\Database\Seeder;

class FacultiesSeeder extends Seeder
{
    public function run(): void
    {
        Faculty::query()->create([
            'name' => 'APT',
        ]);

        Faculty::query()->create([
            'name' => 'Fakultas Ekonomi & Bisnis',
        ]);

        Faculty::query()->create([
            'name' => 'Fakultas Teknik',
        ]);

        Faculty::query()->create([
            'name' => 'Fakultas Ilmu Budaya',
        ]);

        Faculty::query()->create([
            'name' => 'Fakultas Desain Komunikasi & Visual',
        ]);

        Faculty::query()->create([
            'name' => 'Fakultas Ilmu Sosial & Politik',
        ]);
    }
}
