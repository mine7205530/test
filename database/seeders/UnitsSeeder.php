<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Enums\Degree;
use App\Models\Faculty;
use App\Models\Unit;
use Illuminate\Database\Seeder;

class UnitsSeeder extends Seeder
{
    public function run(): void
    {
        $APT = Faculty::query()->where('name', '=', 'APT')->first();
        Unit::create([
            'name' => 'APT',
            'degree' => Degree::PT,
            'email' => 'utama@widyatama.ac.id',
            'faculty_id' => $APT->id,
            'sk_number' => 'SK-0000/UN.5.1/PT/2021',
        ]);

        $FEB = Faculty::query()->where('name', '=', 'Fakultas Ekonomi & Bisnis')->first();
        Unit::create([
            'name' => 'Akutansi',
            'degree' => Degree::S1,
            'email' => 'akutansi.s1@widyatama.ac.id',
            'faculty_id' => $FEB->id,
            'sk_number' => 'SK-0001/UN.5.1/PT/2021',
        ]);
        Unit::create([
            'name' => 'Akutansi',
            'degree' => Degree::D3,
            'email' => 'akutansi.d3@widyatama.ac.id',
            'faculty_id' => $FEB->id,
            'sk_number' => 'SK-0002/UN.5.1/PT/2021',
        ]);
        Unit::create([
            'name' => 'Akutansi',
            'degree' => Degree::PPAK,
            'email' => 'akutansi.ppak@widyatama.ac.id',
            'faculty_id' => $FEB->id,
            'sk_number' => 'SK-0003/UN.5.1/PT/2021',
        ]);
        Unit::create([
            'name' => 'Manajemen',
            'degree' => Degree::S1,
            'email' => 'manajemen.s1@widyatama.ac.id',
            'faculty_id' => $FEB->id,
            'sk_number' => 'SK-0004/UN.5.1/PT/2021',
        ]);
        Unit::create([
            'name' => 'Manajemen',
            'degree' => Degree::D3,
            'email' => 'manajemen.d3@widyatama.ac.id',
            'faculty_id' => $FEB->id,
            'sk_number' => 'SK-0005/UN.5.1/PT/2021',
        ]);
        Unit::create([
            'name' => 'Perdagangan Internasional',
            'degree' => Degree::S1,
            'email' => 'pedagen.s1@widyatama.ac.id',
            'faculty_id' => $FEB->id,
            'sk_number' => 'SK-0006/UN.5.1/PT/2021',
        ]);
        Unit::create([
            'name' => 'Manajemen',
            'degree' => Degree::S2,
            'email' => 'manajemen.s2@widyatama.ac.id',
            'faculty_id' => $FEB->id,
            'sk_number' => 'SK-0007/UN.5.1/PT/2021',
        ]);

        $FT = Faculty::query()->where('name', '=', 'Fakultas Teknik')->first();
        Unit::create([
            'name' => 'Teknik Informatika',
            'degree' => Degree::S1,
            'email' => 'informatika.s1@widyatama.ac.id',
            'faculty_id' => $FT->id,
            'sk_number' => 'SK-0008/UN.5.1/PT/2021',
        ]);
        Unit::create([
            'name' => 'Teknik Industri',
            'degree' => Degree::S1,
            'email' => 'industri.s1@widyatama.ac.id',
            'faculty_id' => $FT->id,
            'sk_number' => 'SK-0009/UN.5.1/PT/2021',
        ]);
        Unit::create([
            'name' => 'Sistem Informasi',
            'degree' => Degree::S1,
            'email' => 'informasi.s1@widyatama.ac.id',
            'faculty_id' => $FT->id,
            'sk_number' => 'SK-0010/UN.5.1/PT/2021',
        ]);
        Unit::create([
            'name' => 'Teknik Elektro',
            'degree' => Degree::S1,
            'email' => 'elektro.s1@widyatama.ac.id',
            'faculty_id' => $FT->id,
            'sk_number' => 'SK-0011/UN.5.1/PT/2021',
        ]);
        Unit::create([
            'name' => 'Teknik Mesin',
            'degree' => Degree::S1,
            'email' => 'mesin.s1@widyatama.ac.id',
            'faculty_id' => $FT->id,
            'sk_number' => 'SK-0012/UN.5.1/PT/2021',
        ]);
        Unit::create([
            'name' => 'Teknik Mesin',
            'degree' => Degree::D3,
            'email' => 'mesin.d3@widyatama.ac.id',
            'faculty_id' => $FT->id,
            'sk_number' => 'SK-0013/UN.5.1/PT/2021',
        ]);
        Unit::create([
            'name' => 'Teknik Sipil',
            'degree' => Degree::S1,
            'email' => 'sipil.s1@widyatama.ac.id',
            'faculty_id' => $FT->id,
            'sk_number' => 'SK-0014/UN.5.1/PT/2021',
        ]);

        $FIB = Faculty::query()->where('name', '=', 'Fakultas Ilmu Budaya')->first();
        Unit::create([
            'name' => 'Bahasa Jepang',
            'degree' => Degree::S1,
            'email' => 'b.jepang.s1@widyatama.ac.id',
            'faculty_id' => $FIB->id,
            'sk_number' => 'SK-0015/UN.5.1/PT/2021',
        ]);
        Unit::create([
            'name' => 'Bahasa Jepang',
            'degree' => Degree::D3,
            'email' => 'b.jepang.d3@widyatama.ac.id',
            'faculty_id' => $FIB->id,
            'sk_number' => 'SK-0016/UN.5.1/PT/2021',
        ]);
        Unit::create([
            'name' => 'Bahasa Inggris',
            'degree' => Degree::S1,
            'email' => 'b.inggris.s1@widyatama.ac.id',
            'faculty_id' => $FIB->id,
            'sk_number' => 'SK-0017/UN.5.1/PT/2021',
        ]);

        $FDKV = Faculty::query()->where('name', '=', 'Fakultas Desain Komunikasi & Visual')->first();
        Unit::create([
            'name' => 'Desain Grafis',
            'degree' => Degree::D4,
            'email' => 'desain.grafis.s1@widyatama.ac.id',
            'faculty_id' => $FDKV->id,
            'sk_number' => 'SK-0018/UN.5.1/PT/2021',
        ]);
        Unit::create([
            'name' => 'Multimedia',
            'degree' => Degree::D3,
            'email' => 'multimedia.s1@widyatama.ac.id',
            'faculty_id' => $FDKV->id,
            'sk_number' => 'SK-0019/UN.5.1/PT/2021',
        ]);

        $FISIP = Faculty::query()->where('name', '=', 'Fakultas Ilmu Sosial & Politik')->first();
        Unit::create([
            'name' => 'Perpustakaan & Sains Informasi',
            'degree' => Degree::S1,
            'email' => 'perpus.s1@widyatama.ac.id',
            'faculty_id' => $FISIP->id,
            'sk_number' => 'SK-0020/UN.5.1/PT/2021',
        ]);
        Unit::create([
            'name' => 'Produksi Film & Televisi',
            'degree' => Degree::S1,
            'email' => 'film.s1@widyatama.ac.id',
            'faculty_id' => $FISIP->id,
            'sk_number' => 'SK-0021/UN.5.1/PT/2021',
        ]);
    }
}
