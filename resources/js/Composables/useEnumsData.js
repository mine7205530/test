export function useEnumsData() {
  const degrees = [
    { id: 1, name: "D3" },
    { id: 2, name: "D4" },
    { id: 3, name: "S1" },
    { id: 4, name: "S2" },
    { id: 5, name: "S3" },
    { id: 6, name: "PT" },
    { id: 7, name: "PPAk" }
  ]

  const grades = [
    { id: 1, name: "A" },
    { id: 2, name: "B" },
    { id: 3, name: "C" },
    { id: 4, name: "Unggul" },
    { id: 5, name: "Baik Sekali" },
    { id: 6, name: "Baik" }
  ]

  const institutions = [
    { id: 1, name: "BAN-PT" },
    { id: 2, name: "LAMEMBA" },
    { id: 3, name: "LAMINFOKOM" },
    { id: 4, name: "LAMTEKNIK" },
    { id: 5, name: "LAMDIK" },
    { id: 6, name: "LAMSAMA" }
  ]

  return {
    degrees,
    grades,
    institutions
  }
}
