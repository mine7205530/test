import { reactive, watch } from "vue"
import { router } from "@inertiajs/vue3"

export function useFilters(initialFilters, url) {
  const filters = reactive(initialFilters)

  watch(filters, () => {
    router.get(
      url,
      {
        ...filters
      },
      {
        preserveState: true,
        preserveScroll: true
      }
    )
  })

  return {
    filters
  }
}
