import { ref } from "vue"
import { BellAlertIcon, BuildingOfficeIcon, CalendarDaysIcon, DocumentArrowDownIcon, DocumentChartBarIcon, DocumentCheckIcon, HomeIcon, UsersIcon } from "@heroicons/vue/24/outline/index.js"

function getCurrentModule() {
  if (route().current("modules.accreditations.*")) return "accreditations"
  if (route().current("modules.notifications.*")) return "notifications"
  if (route().current("modules.users.*")) return "users"
}

function isActive(routeName) {
  return route().current(routeName)
}

function loadNavigation(module) {
  if (module === "accreditations") {
    return [
      {
        name: "Dashboard",
        href: route("modules.accreditations.index"),
        icon: HomeIcon,
        current: isActive("modules.accreditations.index")
      },
      {
        name: "Akreditasi Berjalan",
        href: route("modules.accreditations.live.index"),
        icon: DocumentCheckIcon,
        current: isActive("modules.accreditations.live.index")
      },
      {
        name: "Akreditasi Histori",
        href: route("modules.accreditations.history.index"),
        icon: DocumentArrowDownIcon,
        current: isActive("modules.accreditations.history.index")
      },
      {
        name: "Unit",
        href: route("modules.accreditations.unit.index"),
        icon: BuildingOfficeIcon,
        current: isActive("modules.accreditations.unit.index")
      }
    ]
  } else if (module === "notifications") {
    return [
      {
        name: "Dashboard",
        href: route("modules.notifications.index"),
        icon: HomeIcon,
        current: isActive("modules.notifications.index")
      },
      {
        name: "Jadwal Notifikasi",
        href: route("modules.notifications.schedule"),
        icon: CalendarDaysIcon,
        current: isActive("modules.notifications.schedule")
      }
    ]
  } else if (module === "users") {
    return [
      {
        name: "Dashboard",
        href: route("modules.users.index"),
        icon: HomeIcon,
        current: isActive("modules.users.index")
      }
    ]
  }
}

export function useNavigation() {
  const sidebarOpen = ref(false)
  const navigation = ref(loadNavigation(getCurrentModule()))

  const referensi = ref([
    { id: 1, name: "Dokumen SPMI", href: "#", initial: "DS", current: false },
    { id: 2, name: "Prosedur", href: "#", initial: "P", current: false },
    { id: 3, name: "SK Internal", href: "#", initial: "SI", current: false }
  ])

  const userNavigation = ref([
    { name: "Modul", href: route("dashboard"), method: "GET" },
    { name: "Logout", href: route("logout"), method: "POST" }
  ])

  const modulNavigation = ref([
    {
      title: "Modul Akreditasi",
      description: "Kelola Akreditasi Secara Efektif, Efisien, dan Terstruktur.",
      icon: DocumentChartBarIcon,
      background: "bg-pink-500",
      href: route("modules.accreditations.index")
    },
    {
      title: "Modul Notifikasi",
      description: "Kelola Notifikasi Akreditasi Dengan Mudah dan Realtime.",
      icon: BellAlertIcon,
      background: "bg-yellow-500",
      href: route("modules.notifications.index")
    },
    {
      title: "Modul Pengguna",
      description: "Kelola Seluruh Pengguna Dalam Sistem.",
      icon: UsersIcon,
      background: "bg-green-500",
      href: route("modules.users.index")
    }
  ])

  return {
    navigation,
    modulNavigation,
    referensi,
    userNavigation,
    sidebarOpen
  }
}
