<x-mail::message>
# Halo {{ $unit->name }},

Program Studi {{ $unit->name }} telah ditambahkan ke dalam Sistem Penjaminan Mutu Widyatama.

Terima Kasih,<br>
{{ config('app.name') }}
</x-mail::message>
