<x-mail::message>
# Halo {{ $notification->accreditation->unit->name }}

Masa kadaluarsa Perguruan Tinggi akan berakhir pada {{ $notification->due_date->addYear()->toDateString() }}

Terima Kasih,<br>
{{ config('app.name') }}
</x-mail::message>
