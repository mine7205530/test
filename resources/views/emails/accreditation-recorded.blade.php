<x-mail::message>
# Halo {{ $accreditation->unit->name  }},

Akreditasi {{ $accreditation->unit->name }} telah Aktif, berlaku hingga {{ $accreditation->validity_date->toDateString() }}.

Terima Kasih,<br>
{{ config('app.name') }}
</x-mail::message>
