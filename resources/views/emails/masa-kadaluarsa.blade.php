<x-mail::message>
# Halo {{ $notification->accreditation->unit->name }}

Masa berlaku Akreditasi anda akan berakhir pada {{ $notification->accreditation->validity_date->locale('id_ID')->toDateString() }}.

Terima Kasih,<br>
{{ config('app.name') }}
</x-mail::message>
