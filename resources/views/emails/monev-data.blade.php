<x-mail::message>
# Halo {{ $notification->accreditation->unit->name }}

Semester akan segera berakhir, silahkan lakukan Monev Data Akreditasi anda.

Terima Kasih,<br>
{{ config('app.name') }}
</x-mail::message>
