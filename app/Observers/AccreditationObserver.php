<?php

declare(strict_types=1);

namespace App\Observers;

use App\Enums\StatusAccreditation;
use App\Models\Accreditation;
use App\Notifications\AccreditationRecorded;
use Illuminate\Support\Facades\Notification;

class AccreditationObserver
{
    /**
     * Handle the Accreditation "created" event.
     */
    public function created(Accreditation $accreditation): void
    {
        Notification::send(
            $accreditation->unit,
            new AccreditationRecorded(
                $accreditation->load(['unit']),
            ),
        );
        $accreditation->unit()->update([
            'status' => StatusAccreditation::AKTIF,
        ]);
    }

    /**
     * Handle the Accreditation "updated" event.
     */
    public function updated(Accreditation $accreditation): void
    {
        $statusAccreditation = $accreditation->status;
        $accreditation->unit()->update([
            'status' => $statusAccreditation,
        ]);
    }

    /**
     * Handle the Accreditation "deleted" event.
     */
    public function deleted(Accreditation $accreditation): void
    {
        $accreditation->unit()->update([
            'status' => StatusAccreditation::TIDAK_AKTIF,
        ]);
    }

    /**
     * Handle the Accreditation "restored" event.
     */
    public function restored(Accreditation $accreditation): void
    {
        //
    }

    /**
     * Handle the Accreditation "force deleted" event.
     */
    public function forceDeleted(Accreditation $accreditation): void
    {
        //
    }
}
