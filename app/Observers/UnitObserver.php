<?php

declare(strict_types=1);

namespace App\Observers;

use App\Models\Unit;
use App\Notifications\UnitAdded;
use Illuminate\Support\Facades\Notification;

class UnitObserver
{
    /**
     * Handle the Unit "created" event.
     */
    public function created(Unit $unit): void
    {
        Notification::send(
            $unit,
            new UnitAdded(
                $unit->load(['faculty', 'accreditations']),
            ),
        );
    }

    /**
     * Handle the Unit "updated" event.
     */
    public function updated(Unit $unit): void
    {
        //
    }

    /**
     * Handle the Unit "deleted" event.
     */
    public function deleted(Unit $unit): void
    {
        //
    }

    /**
     * Handle the Unit "restored" event.
     */
    public function restored(Unit $unit): void
    {
        //
    }

    /**
     * Handle the Unit "force deleted" event.
     */
    public function forceDeleted(Unit $unit): void
    {
        //
    }
}
