<?php

declare(strict_types=1);

namespace App\Data;

use App\Enums\StatusAccreditation;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Attributes\WithCast;
use Spatie\LaravelData\Casts\EnumCast;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;
use Spatie\LaravelData\Optional;

#[MapName(SnakeCaseMapper::class)]
class UnitData extends Data
{
    public function __construct(
        public string|Optional $name,
        public string|null|Optional $description,
        public string|Optional $email,
        public string|Optional $sk_number,
        public string|Optional $sk_unit,
        public string|Optional $degree,
        #[WithCast(EnumCast::class)]
        public StatusAccreditation|Optional $status,
        public string|Optional $facultyId,
    ) {
        //
    }
}
