<?php

declare(strict_types=1);

namespace App\Data;

use Spatie\LaravelData\Data;
use Spatie\LaravelData\Optional;

class UnitFilterData extends Data
{
    public function __construct(
        public string|Optional $search,
        public string|Optional $sort,
    ) {
        //
    }
}
