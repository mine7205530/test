<?php

declare(strict_types=1);

namespace App\Models;

use App\Enums\Degree;
use App\Enums\StatusAccreditation;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Notifications\Notifiable;

class Unit extends Model
{
    use HasUuids, Notifiable;

    protected $guarded = [];

    protected $casts = [
        'degree' => Degree::class,
        'status' => StatusAccreditation::class,
    ];

    public function faculty(): BelongsTo
    {
        return $this->belongsTo(Faculty::class);
    }

    public function accreditations(): HasMany
    {
        return $this->hasMany(Accreditation::class);
    }

    public function notifications(): HasManyThrough
    {
        return $this->hasManyThrough(Notification::class, Accreditation::class);
    }
}
