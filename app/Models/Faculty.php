<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Faculty extends Model
{
    use HasUuids;

    protected $guarded = [];

    public function units(): HasMany
    {
        return $this->hasMany(Unit::class);
    }

    public function accreditations(): HasManyThrough
    {
        return $this->hasManyThrough(Accreditation::class, Unit::class);
    }
}
