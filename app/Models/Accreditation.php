<?php

declare(strict_types=1);

namespace App\Models;

use App\Enums\GradeAccreditation;
use App\Enums\Institution;
use App\Enums\StatusAccreditation;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Accreditation extends Model
{
    use HasUuids;

    protected $guarded = [];

    protected $casts = [
        'grade' => GradeAccreditation::class,
        'institution' => Institution::class,
        'status' => StatusAccreditation::class,
        'validity_date' => 'date',
    ];

    public function unit(): BelongsTo
    {
        return $this->belongsTo(Unit::class);
    }

    public function notifications(): HasMany
    {
        return $this->hasMany(Notification::class);
    }
}
