<?php

declare(strict_types=1);

namespace App\Models;

use App\Enums\StatusNotification;
use App\Enums\TypeNotification;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasUuids;

    protected $guarded = [];

    protected $casts = [
        'type' => TypeNotification::class,
        'status' => StatusNotification::class,
        'due_date' => 'date',
    ];

    public function accreditation()
    {
        return $this->belongsTo(Accreditation::class);
    }
}
