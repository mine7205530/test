<?php

declare(strict_types=1);

namespace App\Enums;

enum GradeAccreditation: string
{
    case A = 'A';
    case B = 'B';
    case C = 'C';
    case Unggul = 'Unggul';
    case BaikSekali = 'Baik Sekali';
    case Baik = 'Baik';
}
