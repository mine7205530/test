<?php

declare(strict_types=1);

namespace App\Enums;

enum Institution: string
{
    case BAN_PT = 'BAN-PT';
    case LAMEMBA = 'LAMEMBA';
    case LAMINFOKOM = 'LAMINFOKOM';
    case LAMTEKNIK = 'LAMTEKNIK';
    case LAMDIK = 'LAMDIK';
    case LAMSAMA = 'LAMSAMA';
}
