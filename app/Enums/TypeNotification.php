<?php

declare(strict_types=1);

namespace App\Enums;

enum TypeNotification: string
{
    case MASA_KADALUARSA_PT = 'Masa Kadaluarsa PT';
    case MASA_KADALUARSA = 'Masa Kadaluarsa';
    case MONEV_DATA = 'Monev Data';
    case PERSIAPAN_PEMANTAUAN = 'Persiapan Pemantauan';
    case HASIL_PEMANTAUAN = 'Hasil Pemantauan';
    case PERSIAPAN_REAKREDITASI = 'Persiapan Reakreditasi';
}
