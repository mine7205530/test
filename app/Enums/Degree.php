<?php

declare(strict_types=1);

namespace App\Enums;

enum Degree: string
{
    case D3 = 'D3';
    case D4 = 'D4';
    case S1 = 'S1';
    case S2 = 'S2';
    case S3 = 'S3';
    case PT = 'PT';
    case PPAK = 'PPAk';
}
