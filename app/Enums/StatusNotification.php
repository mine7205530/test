<?php

declare(strict_types=1);

namespace App\Enums;

enum StatusNotification: string
{
    case TERKIRIM = 'Terkirim';
    case BELUM_TERKIRIM = 'Belum Terkirim';
    case GAGAL_TERKIRIM = 'Gagal Terkirim';
}
