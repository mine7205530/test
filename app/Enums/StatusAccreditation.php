<?php

declare(strict_types=1);

namespace App\Enums;

enum StatusAccreditation: string
{
    case AKTIF = 'Aktif';
    case TIDAK_AKTIF = 'Tidak Aktif';
    case PANTAU_I = 'Pantau I';
    case PANTAU_II = 'Pantau II';
    case PANTAU_III = 'Pantau III';
}
