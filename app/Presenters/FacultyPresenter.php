<?php

declare(strict_types=1);

namespace App\Presenters;

use AdditionApps\FlexiblePresenter\FlexiblePresenter;

class FacultyPresenter extends FlexiblePresenter
{
    public function values(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
