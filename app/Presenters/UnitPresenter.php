<?php

declare(strict_types=1);

namespace App\Presenters;

use AdditionApps\FlexiblePresenter\FlexiblePresenter;

class UnitPresenter extends FlexiblePresenter
{
    public function values(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'degree' => $this->degree,
            'status' => $this->status,
            'email' => $this->email,
            'sk_unit' => $this->sk_unit,
            'sk_number' => $this->sk_number,
            'faculty' => FacultyPresenter::make($this->faculty),
        ];
    }
}
