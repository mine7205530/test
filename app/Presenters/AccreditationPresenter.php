<?php

declare(strict_types=1);

namespace App\Presenters;

use AdditionApps\FlexiblePresenter\FlexiblePresenter;

class AccreditationPresenter extends FlexiblePresenter
{
    public function values(): array
    {
        return [
            'id' => $this->id,
            'sk_number' => $this->sk_number,
            'grade' => $this->grade,
            'institution' => $this->institution,
            'status' => $this->status,
            'validity_date' => $this->validity_date,
            'sk_accreditation' => $this->sk_accreditation,
            'unit' => fn () => UnitPresenter::make($this->unit)->only('name', 'email', 'degree', 'faculty'),
        ];
    }
}
