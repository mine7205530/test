<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Data\UnitData;
use App\Data\UnitFilterData;
use App\Http\Requests\CreateUnitRequest;
use App\Http\Requests\UpdateUnitRequest;
use App\Models\Unit;
use App\Presenters\FacultyPresenter;
use App\Presenters\UnitPresenter;
use App\Services\FacultyService;
use App\Services\UnitService;
use Illuminate\Http\Request;
use RahulHaque\Filepond\Facades\Filepond;

class UnitController extends Controller
{
    public function __construct(
        private readonly UnitService $unitService,
        private readonly FacultyService $facultyService,
    ) {
        //
    }

    public function index(Request $request)
    {
        $filters = UnitFilterData::from([
            'sort' => $request->string('sort', 'name'),
            'search' => $request->string('search', ''),
        ]);

        $units = $this->unitService->getAllUnit(
            $filters,
            true,
        );

        $faculties = $this->facultyService->getAllFaculty();

        return inertia('Modules/Akreditasi/Unit/Index', [
            'units' => UnitPresenter::collection($units),
            'faculties' => FacultyPresenter::collection($faculties),
            'filters' => $filters,
        ]);
    }

    public function store(CreateUnitRequest $request)
    {
        $fileInfo = Filepond::field($request->sk_unit)->moveTo('sk/unit-'.$request->name.'-'.$request->sk_number);

        $unitData = array_merge($request->all(), [
            'sk_unit' => $fileInfo['location'],
        ]);

        $this->unitService->createNewUnit(UnitData::from($unitData));

        return redirect(route('modules.accreditations.unit.index'))->with([
            'flash.success' => 'Unit berhasil ditambahkan.',
        ]);
    }

    public function destroy(Unit $unit)
    {
        $unit->delete();

        return redirect(route('modules.accreditations.unit.index'))->with([
            'flash.success' => 'Unit berhasil dihapus.',
        ]);
    }

    public function update(Unit $unit, UpdateUnitRequest $request)
    {
        $this->unitService->updateUnit(
            $unit,
            UnitData::from($request->all()),
        );

        return redirect(route('modules.accreditations.unit.index'))->with([
            'flash.success' => 'Unit berhasil diperbarui.',
        ]);
    }
}
