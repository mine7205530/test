<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Data\AccreditationFilterData;
use App\Models\Accreditation;
use App\Presenters\AccreditationPresenter;
use App\Services\AccreditationService;
use Illuminate\Http\Request;

class HistoryAccreditationController extends Controller
{
    public function __construct(
        private readonly AccreditationService $accreditationService,
    ) {
        //
    }

    public function index(Request $request)
    {
        $filters = AccreditationFilterData::from([
            'sort' => $request->string('sort', 'validity_date'),
            'search' => $request->string('search', ''),
        ]);

        $accreditations = $this->accreditationService->getAllHistory($filters, true);

        return inertia('Modules/Akreditasi/Histori/Index', [
            'accreditations' => AccreditationPresenter::collection($accreditations),
            'filters' => $filters,
        ]);
    }

    public function destroy(Accreditation $accreditation)
    {
        $this->accreditationService->deleteAccreditation($accreditation);

        return redirect(route('modules.accreditations.history.index'))
            ->with('success', 'Berhasil menghapus data akreditasi');
    }
}
