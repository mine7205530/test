<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Data\AccreditationFilterData;
use App\Enums\StatusAccreditation;
use App\Http\Requests\RecordNewAccreditationRequest;
use App\Http\Requests\UpdateAccreditationRequest;
use App\Models\Accreditation;
use App\Presenters\AccreditationPresenter;
use App\Services\AccreditationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use RahulHaque\Filepond\Facades\Filepond;

class AccreditationController extends Controller
{
    public function __construct(
        private readonly AccreditationService $accreditationService,
    ) {
        //
    }

    public function index(Request $request)
    {
        $filters = AccreditationFilterData::from([
            'sort' => $request->string('sort', 'validity_date'),
            'search' => $request->string('search', ''),
        ]);

        $accreditations = $this->accreditationService->getAllActiveAccreditation($filters, true);
        $inActiveUnit = $this->accreditationService->getAllInactiveAccreditation();

        return inertia('Modules/Akreditasi/Berjalan/Index', [
            'accreditations' => AccreditationPresenter::collection($accreditations),
            'inActiveUnit' => $inActiveUnit,
            'filters' => $filters,
        ]);
    }

    public function store(RecordNewAccreditationRequest $request)
    {
        $fileInfo = Filepond::field($request->get('sk_accreditation'))
            ->moveTo('sk/Akreditasi-'.$request->get('institution').'-'.$request->get('sk_number'));

        Accreditation::create([
            'unit_id' => $request->get('unit_id'),
            'institution' => $request->get('institution'),
            'sk_number' => $request->get('sk_number'),
            'sk_accreditation' => $fileInfo['location'],
            'grade' => $request->get('grade'),
            'status' => StatusAccreditation::AKTIF,
            'validity_date' => $request->get('validity_date'),
        ]);

        Session::flash('flash.success', 'Berhasil mencatat akreditasi ke dalam sistem');

        return Redirect::back();
    }

    public function update(Accreditation $accreditation, UpdateAccreditationRequest $request)
    {
        //
    }

    public function destroy(Accreditation $accreditation)
    {
        $this->accreditationService->deleteAccreditation($accreditation);

        return redirect(route('modules.accreditations.live.index'))->with([
            'flash.success' => 'Berhasil menghapus akreditasi.',
        ]);
    }

    public function deadactive(Accreditation $accreditation)
    {
        $this->accreditationService->deactivateAccreditation($accreditation);

        return redirect(route('modules.accreditations.live.index'))->with([
            'flash.success' => 'Berhasil menonaktifkan akreditasi.',
        ]);
    }
}
