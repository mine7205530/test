<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Enums\StatusAccreditation;
use App\Enums\StatusNotification;
use App\Enums\TypeNotification;
use App\Http\Requests\ScheduleMonevRequest;
use App\Models\Accreditation;
use App\Models\Notification;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function schedule(Request $request)
    {
        $filters = [
            'search' => $request->get('search', ''),
            'sort' => $request->get('sort', 'due_date'),
        ];

        $notifications = Notification::with(['accreditation.unit'])
            ->when($request->get('search', ''), function (Builder $query, string $search) {
                $query->whereHas('accreditation.unit', fn (Builder $query) => $query->where('name', 'LIKE', "%{$search}%")
                    ->orWhere('degree', 'LIKE', "%{$search}%")
                );
            })
            ->when($request->get('sort', 'due_date'), function (Builder $query, string $sort) {
                if ($sort === 'name') {
                    $query->whereHas('accreditation.unit', fn (Builder $query) => $query->orderBy('name'));
                } else {
                    $query->orderBy($sort);
                }
            })
            ->paginate(10)
            ->withQueryString();

        return inertia('Modules/Notifikasi/Jadwal', [
            'notifications' => $notifications,
            'filters' => $filters,
        ]);
    }

    public function scheduleMonev(ScheduleMonevRequest $request)
    {
        $accreditations = Accreditation::whereNot('status', '=', StatusAccreditation::TIDAK_AKTIF)
            ->get();

        foreach ($accreditations as $accreditation) {
            $accreditation->notifications()->create([
                'status' => StatusNotification::BELUM_TERKIRIM,
                'type' => TypeNotification::MONEV_DATA,
                'due_date' => $request->get('due_date'),
            ]);
        }

        return redirect()->back()->with('success', 'Berhasil menambahkan jadwal monev');
    }
}
