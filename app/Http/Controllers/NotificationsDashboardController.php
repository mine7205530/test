<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationsDashboardController extends Controller
{
    public function __invoke(Request $request)
    {
        return inertia('Modules/Notifikasi/Index');
    }
}
