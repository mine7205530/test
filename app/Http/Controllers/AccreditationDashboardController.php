<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Enums\StatusAccreditation;
use App\Models\Accreditation;
use App\Models\Unit;
use Illuminate\Support\Facades\DB;

class AccreditationDashboardController extends Controller
{
    public function __invoke()
    {
        return inertia('Modules/Akreditasi/Index', [
            'tingkat_akreditasi' => $this->getTingkatAkreditasi(),
            'ringkasan_akreditasi' => $this->getRingkasanAkreditasi(),
            'inactiveUnit' => $this->getInactiveUnit(),
        ]);
    }

    private function getInactiveUnit()
    {
        return Unit::with(['accreditations'])->where('status', '=', StatusAccreditation::TIDAK_AKTIF)
            ->get()->sortBy('accreditations.validity_date');
    }

    private function getRingkasanAkreditasi()
    {
        $unitCounts = Unit::select('status', DB::raw('COUNT(*) as total'))
            ->groupBy('status')
            ->get();

        $labels = $unitCounts->pluck('status')->toArray();
        $data = $unitCounts->pluck('total')->toArray();

        return [
            'labels' => $labels,
            'datasets' => [
                [
                    'data' => $data,
                ],
            ],
        ];
    }

    private function getTingkatAkreditasi()
    {
        $gradeCounts = Accreditation::select('grade', DB::raw('COUNT(*) as total'))
            ->groupBy('grade')
            ->get();

        $labels = $gradeCounts->pluck('grade')->toArray();
        $data = $gradeCounts->pluck('total')->toArray();

        return [
            'labels' => $labels,
            'datasets' => [
                [
                    'data' => $data,
                ],
            ],
        ];
    }
}
