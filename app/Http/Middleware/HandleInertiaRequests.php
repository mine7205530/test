<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Presenters\UserPresenter;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use Inertia\Middleware;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that's loaded on the first page visit.
     *
     * @see https://inertiajs.com/server-side-setup#root-template
     *
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determines the current asset version.
     *
     * @see https://inertiajs.com/asset-versioning
     */
    public function version(Request $request): ?string
    {
        return parent::version($request);
    }

    /**
     * Defines the props that are shared by default.
     *
     * @see https://inertiajs.com/shared-data
     */
    public function share(Request $request): array
    {
        Inertia::share('csrf_token', fn () => csrf_token());
        Inertia::share('breadcrumbs', fn () => Route::has($request->route()->getName()) ? Breadcrumbs::generate($request->route()->getName()) : []);
        Inertia::share('user', fn () => $request->user() ? UserPresenter::make($request->user()) : null);
        Inertia::share('flash', function () use ($request) {
            return [
                'info' => fn () => $request->session()->get('info'),
                'success' => fn () => $request->session()->get('success'),
                'error' => fn () => $request->session()->get('error'),
            ];
        });

        return array_merge(parent::share($request), [
            //
        ]);
    }
}
