<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RecordNewAccreditationRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'unit_id' => ['uuid', 'required'],
            'grade' => ['required', 'string'],
            'institution' => ['required', 'string'],
            'validity_date' => ['required', 'date', 'after:today'],
            'sk_number' => ['required', 'string', 'max:50', 'unique:accreditations,sk_number'],
            'sk_accreditation' => ['required', Rule::filepond(['max:10240', 'mimes:pdf'])],
        ];
    }
}
