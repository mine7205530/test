<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateUnitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'faculty_id' => ['required', 'exists:faculties,id', 'uuid'],
            'name' => ['required', 'string', 'max:50', 'min:10'],
            'description' => ['string', 'nullable'],
            'degree' => ['required', 'string', 'max:5'],
            'status' => ['sometimes', 'string', 'max:50'],
            'email' => ['required', 'string', 'email', 'max:50', 'ends_with:@widyatama.ac.id', 'unique:units,email'],
            'sk_number' => ['required', 'string', 'max:50', 'unique:units,sk_number'],
            'sk_unit' => ['required', Rule::filepond(['max:10240', 'mimes:pdf'])],
        ];
    }
}
