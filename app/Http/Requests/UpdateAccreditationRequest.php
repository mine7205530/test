<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAccreditationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'sk_number' => ['required', 'max:50', 'string', 'unique:accreditations,sk_number,'.$this->route('accreditation')->id],
            'grade' => ['required', 'string', 'in:D3,D4,S1,S2,S3,PPAk,PT'],
            'institution' => ['required', 'string', 'in:BAN-PT,LAMEMBA,LAMINFOKOM,LAMTEKNIK,LAMDIK,LAMSAMA'],
            'status' => ['required', 'string', 'in:Aktif,Tidak Aktif,Pantau I,Pantau II,Pantau III'],
            'validity_date' => ['required', 'date', 'after:today'],
            'file' => ['nullable', 'file', 'max:3072', 'mimes:pdf'],
            'unit_id' => ['required', 'exists:units,id'],
        ];
    }
}
