<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Enums\StatusNotification;
use App\Enums\TypeNotification;
use App\Models\Notification;
use App\Notifications\MasaKadaluarsa;
use App\Notifications\MasaKadaluarsaPT;
use App\Notifications\MonevData;
use Illuminate\Console\Command;

class AutoSendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:auto-send-emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send emails notifications automatically';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $notifications = Notification::where('due_date', '=', date('Y-m-d'))->whereNot('status', '=', StatusNotification::TERKIRIM)->get();

        foreach ($notifications as $notification) {
            switch ($notification->type) {
                case TypeNotification::MASA_KADALUARSA:
                    $notification->accreditation->unit->notify(new MasaKadaluarsa($notification));
                    $notification->update(['status' => StatusNotification::TERKIRIM]);
                    break;
                case TypeNotification::MASA_KADALUARSA_PT:
                    $notification->accreditation->unit->notify(new MasaKadaluarsaPT($notification));
                    $notification->update(['status' => StatusNotification::TERKIRIM]);
                    break;
                case TypeNotification::MONEV_DATA:
                    $notification->accreditation->unit->notify(new MonevData($notification));
                    $notification->update(['status' => StatusNotification::TERKIRIM]);
                    break;
            }
        }
    }
}
