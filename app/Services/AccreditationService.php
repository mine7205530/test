<?php

declare(strict_types=1);

namespace App\Services;

use App\Data\AccreditationFilterData;
use App\Data\UnitData;
use App\Enums\StatusAccreditation;
use App\Models\Accreditation;
use App\Models\Unit;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class AccreditationService
{
    public function __construct(
        private readonly UnitService $unitService,
    ) {
        //
    }

    public function getAllActiveAccreditation(AccreditationFilterData $filters, $paginated = false, int $perPage = 10): Collection|LengthAwarePaginator
    {
        $query = Accreditation::query()
            ->with(['unit'])
            ->where('status', '=', StatusAccreditation::AKTIF);

        $query->when($filters->search, function (Builder $query, string $search) {
            $query->whereHas('unit', function (Builder $query) use ($search) {
                $query->where('name', 'LIKE', "%{$search}%");
            });
        })->when($filters->sort, function (Builder $query, string $sort) {
            if ($sort === 'name') {
                Unit::select('name')
                    ->whereColumn('id', '=', 'accreditations.unit_id')
                    ->orderByDesc($sort)
                    ->limit(1);
            } else {
                $query->orderBy($sort);
            }
        });

        if ($paginated) {
            return $query->paginate($perPage)->withQueryString();
        }

        return $query->get();
    }

    public function deleteAccreditation(Accreditation $accreditation)
    {
        $accreditation->delete();

        $this->unitService->updateUnit($accreditation->unit, UnitData::from([
            'status' => StatusAccreditation::TIDAK_AKTIF,
        ]));
    }

    public function deactivateAccreditation(Accreditation $accreditation)
    {
        $accreditation->update([
            'status' => StatusAccreditation::TIDAK_AKTIF,
        ]);
    }

    public function getAllHistory(AccreditationFilterData $filterData, bool $paginated = false, int $perPage = 10)
    {
        $query = Accreditation::query()
            ->with('unit')
            ->where('status', '=', StatusAccreditation::TIDAK_AKTIF);

        $query->when($filterData->search, function (Builder $query, string $search) {
            $query->whereHas('unit', function (Builder $query) use ($search) {
                $query->where('name', 'LIKE', "%{$search}%");
            });
        })->when($filterData->sort, function (Builder $query, string $sort) {
            if ($sort === 'name') {
                Unit::select('name')
                    ->whereColumn('id', '=', 'accreditations.unit_id')
                    ->orderByDesc($sort)
                    ->limit(1);
            } else {
                $query->orderBy($sort);
            }
        });

        if ($paginated) {
            return $query->simplePaginate($perPage)->withQueryString();
        }

        return $query->get();
    }

    public function getAllInactiveAccreditation(): Collection
    {
        return Unit::query()
            ->whereDoesntHave('accreditations', function (Builder $builder) {
                $builder->where('status', '=', StatusAccreditation::AKTIF);
            })->get();
    }
}
