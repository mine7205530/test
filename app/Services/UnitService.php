<?php

declare(strict_types=1);

namespace App\Services;

use App\Data\UnitData;
use App\Data\UnitFilterData;
use App\Models\Faculty;
use App\Models\Unit;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Spatie\LaravelData\Optional;

class UnitService
{
    public function getAllUnit(UnitFilterData $filterData, bool $paginate = false, int $perPage = 10): Collection|LengthAwarePaginator
    {
        $query = Unit::query()
            ->with(['faculty']);

        $query->when($filterData->search, function (Builder $builder, string|Optional $search) {
            $builder->orWhere('name', 'LIKE', "%{$search}%")
                ->orWhere('email', 'LIKE', "%{$search}%");
        })->when($filterData->sort, function (Builder $builder, string|Optional $sort) {
            if ($sort === 'faculty') {
                Faculty::select('name')
                    ->whereColumn('id', '=', 'units.faculty_id')
                    ->orderByDesc('name')
                    ->limit(1);
            } else {
                $builder->orderBy($sort);
            }
        });

        if ($paginate) {
            return $query->paginate($perPage)->withQueryString();
        }

        return $query->get();
    }

    public function createNewUnit(UnitData $unitData): void
    {
        Unit::query()
            ->create($unitData->toArray());
    }

    public function updateUnit(Unit $unit, UnitData $unitData): void
    {
        $unit->update($unitData->toArray());
    }
}
